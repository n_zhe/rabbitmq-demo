package com.xsxy.rabbitmq.demo.quickStart;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.AMQP.BasicProperties;

/**
 * Procuder
 */
public class Procuder {

    public static void main(String[] args) throws IOException, TimeoutException {
        // 1、创建链接工厂ConnectionFactory
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("59.110.232.8");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("woaini");
        connectionFactory.setPassword("woaini");

        // 2、通过工厂创建connection
        Connection connection = connectionFactory.newConnection();

        // 3、通过connection创建一个Channel
        Channel channel = connection.createChannel();

        String exchange = "";
        String routingKey = "test001";
        BasicProperties props = null;
        String msg = "hello rabbit-mq";
        // 4、通过channel发送数据 发送5次数据
        for (int i = 0; i < 5; i++) {
            channel.basicPublish(exchange, routingKey, props, msg.getBytes());
        }

        System.out.println("已经发送消息了");
        // 5、记得要关闭相关的链接
        channel.close();
        connection.close();
    }

}