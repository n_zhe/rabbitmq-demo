package com.xsxy.rabbitmq.demo.quickStart;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;
import com.rabbitmq.client.QueueingConsumer.Delivery;

/**
 * Consumer
 */
public class Consumer {

    public static void main(String[] args) throws IOException, TimeoutException, ShutdownSignalException,
            ConsumerCancelledException, InterruptedException {
        // 1、创建链接工厂ConnectionFactory
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("59.110.232.8");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("woaini");
        connectionFactory.setPassword("woaini");

        // 1.1 以下两个配置代表是否自动重连） 防止因网络故障导致mq断开
        connectionFactory.setAutomaticRecoveryEnabled(true);
        connectionFactory.setNetworkRecoveryInterval(3000);

        // 2、通过工厂创建connection
        Connection connection = connectionFactory.newConnection();

        // 3、通过connection创建一个Channel
        Channel channel = connection.createChannel();

        // 4、声明一个队列
        // queue 队列的名称 durable 是否持久化 exclusive 是否独占 autoDelete 是否自动删除 arguments
        // 其他的一些参数设置
        String queueName = "test001";
        channel.queueDeclare(queueName, true, false, false, null);

        // 5、常见一个消费者
        QueueingConsumer queueingConsumer = new QueueingConsumer(channel);

        // 6、 设置channel
        channel.basicConsume(queueName, true, queueingConsumer);

        System.out.println("等待获取消息======");
        // 7、获取消息
        while (true) {
            Delivery delivery = queueingConsumer.nextDelivery();
            String body = new String(delivery.getBody());
            System.out.println("消费端" + body);
        }
    }
}