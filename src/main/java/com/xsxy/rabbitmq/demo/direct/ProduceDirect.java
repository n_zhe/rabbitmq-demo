package com.xsxy.rabbitmq.demo.direct;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * ProduceDirect
 */
public class ProduceDirect {

    public static void main(String[] args) throws Exception {
        // 1、创建连接工厂
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("****");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("***");
        connectionFactory.setPassword("***");

        // 2、通过工厂获取连接
        Connection connection = connectionFactory.newConnection();

        // 3、获取通道
        Channel channel = connection.createChannel();

        String exchangeName = "directExchange";
        String exchagenType = "direct";
        String queueName = "test.direct.queue";
        String routingKey = "test.direct";
        // 4、声明交换机
        channel.exchangeDeclare(exchangeName, exchagenType, true, false, null);
        // 5、声明队列
        channel.queueDeclare(queueName, true, false, false, null);
        // 6、简历绑定关系
        channel.queueBind(queueName, exchangeName, routingKey);

        // 7发行消息
        for (int i = 0; i < 5; i++) {

            channel.basicPublish(exchangeName, routingKey, null, "testdirectexchage message".getBytes());
        }

        System.err.println("消息已发送===========");

        channel.close();
        connection.close();
    }
}