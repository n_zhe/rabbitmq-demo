package com.xsxy.rabbitmq.demo.direct;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ConsumerDirect {

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        // 1、创建连接
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("****");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("***");
        connectionFactory.setPassword("***");

        // 2、通过Factory创建连接
        Connection connection = connectionFactory.newConnection();

        // 3、创建通道
        Channel channel = connection.createChannel();

        String queueName = "test.direct.queue";
        // 声明队列
        channel.queueDeclare(queueName, true, false, false, null);

        // 4、创建消费去列
        QueueingConsumer queueingConsumer = new QueueingConsumer(channel);

        // 5、消费消息（非自动确认）
        channel.basicConsume(queueName, false, queueingConsumer);

        // 6、监听消息
        System.out.println("正在监听消息===");
        while (true) {
            QueueingConsumer.Delivery delivery = queueingConsumer.nextDelivery();
            byte[] body = delivery.getBody();
            System.out.println(new String(body));
            // 手动确认
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        }

    }
}
